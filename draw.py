import sdl2.ext
import math


class Drawer:
    LINE_COLOR = sdl2.ext.Color(255, 0, 0, 0)

    def __init__(self, renderer):
        self.renderer = renderer

    def draw(self):
        a = 250
        t = -1.5
        tan_t = math.tan(t)
        x = a * tan_t * tan_t / (1 + tan_t * tan_t)
        y = a * tan_t * tan_t * tan_t / (1 + tan_t * tan_t)
        while t < 1.5:
            prev_x = x
            prev_y = y
            tan_t = math.tan(t)
            x = a * tan_t * tan_t / (1 + tan_t * tan_t)
            y = a * tan_t * tan_t * tan_t / (1 + tan_t * tan_t)
            t = t + 0.1
            self.renderer.draw_line(prev_x, prev_y, x, y, Drawer.LINE_COLOR)
