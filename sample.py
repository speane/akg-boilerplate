import sys
import sdl2
import sdl2.ext
import config
import draw


def run():
    sdl2.ext.init()

    window = create_window()
    window.show()
    application_renderer = ApplicationRenderer(window,
                                               config.WINDOW_WIDTH,
                                               config.WINDOW_HEIGHT,
                                               config.WINDOW_WIDTH / 5,
                                               config.WINDOW_HEIGHT / 2)

    drawer = draw.Drawer(application_renderer)
    running = True
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
                break
            if event.type == sdl2.SDL_WINDOWEVENT:
                if event.window.event == sdl2.SDL_WINDOWEVENT_RESIZED:
                    application_renderer.update_coordinates(event.window.data1,
                                                            event.window.data2)
        application_renderer.render(drawer)
        sdl2.SDL_Delay(100)
    sdl2.ext.quit()
    return 0


def create_window():
    return sdl2.ext.Window(config.WINDOW_TITLE, size=(config.WINDOW_WIDTH,
                                                      config.WINDOW_HEIGHT),
                           flags=sdl2.SDL_WINDOW_RESIZABLE)


class ApplicationRenderer:
    CLEAR_COLOR = sdl2.ext.Color(255, 255, 255, 0)
    AXIS_COLOR = sdl2.ext.Color(0, 0, 0, 255)

    def __init__(self, window, native_width, native_height, left_offset, right_offset):
        self.renderer = sdl2.ext.Renderer(window)

        self.native_width = native_width
        self.native_height = native_height

        self.left_offset = left_offset
        self.top_offset = right_offset

        self.x_scale = 1
        self.y_scale = 1

        self.update_coordinates(native_width, native_height)

    def update_coordinates(self, width, height):
        self.x_scale = width / float(self.native_width)
        self.y_scale = height / float(self.native_height)

    def render(self, drawer):
        self.clear()
        self.draw_axis()
        drawer.draw()
        self.update()

    def clear(self):
        self.renderer.clear(ApplicationRenderer.CLEAR_COLOR)

    def draw_line(self, x1, y1, x2, y2, color):
        self.renderer.draw_line(points=(int(self.x_scale * (x1 + self.left_offset)),
                                        int(self.y_scale * (-y1 + self.top_offset)),
                                        int(self.x_scale * (x2 + self.left_offset)),
                                        int(self.y_scale * (-y2 + self.top_offset))),
                                color=color)

    def draw_axis(self):
        self.draw_line(-self.left_offset,
                       0,
                       self.native_width - self.left_offset,
                       0,
                       ApplicationRenderer.AXIS_COLOR)
        self.draw_line(0,
                       -self.top_offset,
                       0,
                       self.native_height - self.top_offset,
                       ApplicationRenderer.AXIS_COLOR)

    def update(self):
        self.renderer.present()


if __name__ == "__main__":
    sys.exit(run())
